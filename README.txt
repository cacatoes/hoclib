# hoclib - Bibliotheque PureData vanilla 

Ensemble d'objets écrits en puredata à des fins musicales.

## Installation

Ajouter hoclib dans le path de puredata. 

## Conventions

  * Chaque objet est préfixé par hoc.* 
  * pas d'interface graphique.
  * Chaque objet est accompagné par son fichier de documention -help, qui renseigne au minimum:
   * la description (avec un lien vers l'algorithme si nécessaire). 
   * les inlets/outlets
   * les arguments
   * les echelles de valeurs des paramètres
  * l'objet n'a pas de valeur par défaut ni de limitation d'echelle (en cours)

## Licence

L'ensemble est distribué sous licence GPLv3.
